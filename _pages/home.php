---
title: 'main::lang.home.title'
permalink: /
description: ''
layout: default

'[slider]':
    code: home-slider

'[userlogin]': { }

'[localSearch]':
    hideSearch: 0
    menusPage: local/menus

'[featuredItems]':
    items: ['73', '206', '130']
    limit: 3
    itemsPerRow: 3
    itemWidth: 400
    itemHeight: 300
---

<?= component('slider'); ?>

<?php if (!$customer) { ?>
<div class="container">
    <div class="row">
        <div class="col-sm-4 mx-auto">
            <div class="card home-login">
                <div class="card-body">
                    <h1 class="card-title h3 mb-4 font-weight-normal">
                        <?= lang('main::lang.account.login.text_login'); ?>
                    </h1>

                    <?= partial('userlogin::default'); ?>

                    <div class="row">
                        <!-- <div class="col-md-5 p-sm-0">
                            <a class="btn btn-link btn-lg" href="<?= site_url('account/reset'); ?>">
                                <span class="small"><?= lang('main::lang.account.login.text_forgot'); ?></span>
                            </a>
                        </div> -->
                        <?php if ((bool)$canRegister) { ?>
                        <div class="col-sm-7">
                            <a
                                class="btn btn-outline-default btn-block btn-lg"
                                href="<?= site_url('account/register'); ?>"
                            ><?= lang('main::lang.account.login.button_register'); ?>
                            </a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </div>
</div>

<?php }?>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <?= component('featuredItems'); ?>
        </div>
    </div>
</div>

